package com.inacap.ciclo.aplicaciongo.modelo.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by 19796832-K on 25-08-2017.
 */

public class AplicacionGoDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "AplicacionGo.db";
    public static final int DATABASE_VERSION = 1;

    private static final  String SQL_CREATE =
            "CREATE TABLE " + AplicacionGoDBContract.AplicacionGoUsuarios.TABLE_NAME +
                    "(" + AplicacionGoDBContract.AplicacionGoUsuarios._ID +  "INTERGER PRIMARY KEY," +
                    AplicacionGoDBContract.AplicacionGoUsuarios.COLUM_NAME_NAME + "TEXT," +
                    AplicacionGoDBContract.AplicacionGoUsuarios.COLUM_NAME_LASTNAME + "TEXT," +
                    AplicacionGoDBContract.AplicacionGoUsuarios.COLUM_NAME_EMAIL + "TEXT," +
                    AplicacionGoDBContract.AplicacionGoUsuarios.COLUM_NAME_USERNAME + "TEXT," +
                    AplicacionGoDBContract.AplicacionGoUsuarios.COLUM_NAME_PASSWORD + "TEXT,)" ;


    public AplicacionGoDBHelper(Context context) {
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(this.SQL_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}
