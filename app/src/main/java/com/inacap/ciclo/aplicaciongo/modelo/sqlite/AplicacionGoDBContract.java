package com.inacap.ciclo.aplicaciongo.modelo.sqlite;

import android.provider.BaseColumns;

/**
 * Created by 19796832-K on 25-08-2017.
 */

public class AplicacionGoDBContract {
    private AplicacionGoDBContract(){}
    public static class AplicacionGoUsuarios implements BaseColumns{
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUM_NAME_USERNAME = "username";
        public static final String COLUM_NAME_PASSWORD = "password";
        public static final String COLUM_NAME_NAME = "name";
        public static final String COLUM_NAME_LASTNAME = "lastname";
        public static final String COLUM_NAME_EMAIL = "email";

    }
}
