package com.inacap.ciclo.aplicaciongo.modelo.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by 19796832-K on 25-08-2017.
 */

public class UsuariosModel {
    private AplicacionGoDBHelper dbHelper;

    public UsuariosModel(Context context){

        this.dbHelper = new AplicacionGoDBHelper(context);
    }

    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(AplicacionGoDBContract.AplicacionGoUsuarios.TABLE_NAME, null, usuario);

    }

}
