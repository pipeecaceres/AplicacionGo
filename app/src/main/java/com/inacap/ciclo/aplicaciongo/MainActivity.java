package com.inacap.ciclo.aplicaciongo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.inacap.ciclo.aplicaciongo.vista.FormularioActivity;

public class MainActivity extends AppCompatActivity {

    private EditText etUsName,etPassword;
    private Button btLogin;
    private TextView tvUsername,tvRegistrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.etUsName = (EditText) findViewById(R.id.etUsName);
        this.etPassword = (EditText) findViewById(R.id.etPassword);

        this.btLogin = (Button) findViewById(R.id.btLogin);
        this.tvUsername = (TextView) findViewById(R.id.tvUsername);
        this.tvRegistrar = (TextView) findViewById(R.id.tvRegistrar);

        this.btLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Obtener el nombre de usuario
                String nombre_usuario = etUsName.getText().toString();
                //mostrar nombre de usuario
                tvUsername.setText(nombre_usuario);
                //mostrar en toast (mensaje temporal)
                Toast.makeText(getApplicationContext(), "Username: " + nombre_usuario, Toast.LENGTH_SHORT).show();
                //logs
                Log.v("HolaMundo", "El usuario ingreso el nombre: " + nombre_usuario);
            }
        });

        this.tvRegistrar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //iniciar la segunda ventana
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(nuevaVentana);
            }
        });

    }
}
